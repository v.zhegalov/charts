import "./App.css"
import PieChart from "./components/PieChart"
// import XYChart from "./components/XYChart"

function App() {
  return (
    <div className="App">
      <PieChart></PieChart>
      {/* <XYChart></XYChart> */}
    </div>
  )
}

export default App

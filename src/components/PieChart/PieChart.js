import React, { useLayoutEffect } from "react"
import "../../App.css"
import * as am5 from "@amcharts/amcharts5"
import * as am5percent from "@amcharts/amcharts5/percent"
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated"
import ChartTheme from "../../ChartTheme"

import chartData from "./data"

export default function PieChart() {
  useLayoutEffect(() => {
    let root = am5.Root.new("chartdiv")

    // Set themes
    // https://www.amcharts.com/docs/v5/concepts/themes/
    root.setThemes([am5themes_Animated.new(root), ChartTheme.new(root)])

    // Create chart
    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/
    // start and end angle must be set both for chart and series
    let chart = root.container.children.push(
      am5percent.PieChart.new(root, {
        layout: root.verticalLayout,
        innerRadius: am5.percent(40),
      })
    )

    // Create series
    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Series
    // start and end angle must be set both for chart and series

    /**
     * Слой родительских категорий
     * с настройками полей из датасета
     */
    let parentSeries = chart.series.push(
      am5percent.PieSeries.new(root, {
        valueField: "value",
        categoryField: "title",
        legendLabelText: "[{fill}]{category}[/]",
        legendValueText: "",
        alignLabels: false,
        innerRadius: am5.percent(20),
      })
    )

    let bgColor = root.interfaceColors.get("background")

    parentSeries.ticks.template.setAll({ forceHidden: true })
    parentSeries.labels.template.setAll({ forceHidden: true })
    parentSeries.slices.template.setAll({
      stroke: bgColor,
      strokeWidth: 1,
      toggleKey: "none",
    })
    parentSeries.slices.template.states.create("hover", { scale: 0.95 })

    /**
     * Слой "подкатегорий"
     * с настройками полей из датасета
     */
    let subTypeSeries = chart.series.push(
      am5percent.PieSeries.new(root, {
        valueField: "value",
        categoryField: "title",
      })
    )

    subTypeSeries.slices.template.setAll({
      stroke: bgColor,
      strokeWidth: 1,
      toggleKey: "none",
      templateField: "params",
    })
    subTypeSeries.ticks.template.setAll({ forceHidden: true })
    subTypeSeries.labels.template.setAll({ forceHidden: true })

    /**
     * Настройки отображения тултипа
     * можно вынести в отдлеьный модуль
     */
    let tooltip = am5.Tooltip.new(root, {
      getFillFromSprite: false,
      getStrokeFromSprite: false,
      autoTextColor: false,
      getLabelFillFromSprite: false,
      labelText:
        "{category}: [bold]{valuePercentTotal.formatNumber('0.00')}%[/]",
    })

    tooltip.label.setAll({
      fill: am5.color(0x222427),
    })

    tooltip.get("background").setAll({
      fill: am5.color(0xffffff),
      fillOpacity: 1,
    })

    parentSeries.set("tooltip", tooltip)
    subTypeSeries.set("tooltip", tooltip)

    // Set data
    // https://www.amcharts.com/docs/v5/charts/percent-charts/pie-chart/#Setting_data

    /**
     * Тут из общего дата сета берем потомков
     * и просчитываем для них прозрачность
     *
     * Этот набор данных будет отображаться
     * на верхнем слое "подкатегорий"
     */
    const subTypes = chartData.reduce((acc, item) => {
      const children = item.children.map((el, i) => ({
        ...el,
        type: "child",
        params: {
          fill: item.color,
          opacity: (100 - (i * 100) / item.children.length / 1.5) / 100 / 1.2,
        },
      }))
      acc.push(...children)
      return acc
    }, [])

    parentSeries.data.setAll(chartData)
    subTypeSeries.data.setAll(subTypes)
    console.log("pie parent", chartData)
    console.log("pie child", subTypes)

    // Play initial series animation
    // https://www.amcharts.com/docs/v5/concepts/animations/#Animation_of_series
    parentSeries.appear(1000, 100)
    subTypeSeries.appear(1000, 100)

    /**
     * Ностройки отображения легенды,
     * ее позиции и ее маркеров
     */
    var legend = chart.children.push(
      am5.Legend.new(root, {
        nameField: "title",
        fillField: "color",
        x: am5.percent(50),
        y: am5.percent(50),
        centerX: am5.percent(35),
        centerY: am5.percent(100),
        layout: root.verticalLayout,
        clickTarget: "none",
      })
    )
    legend.markers.template.setAll({
      width: 8,
      height: 8,
      useDefaultMarker: true,
    })
    legend.markerRectangles.template.setAll({
      cornerRadiusTL: 10,
      cornerRadiusTR: 10,
      cornerRadiusBL: 10,
      cornerRadiusBR: 10,
    })
    legend.labels.template.setAll({
      fill: "#000000",
    })
    //legend.data.setAll(parentSeries.dataItems)
    legend.data.setAll(chartData)

    return () => {
      root.dispose()
    }
  }, [])

  return <div id="chartdiv" style={{ width: "100%", height: "500px" }}></div>
}

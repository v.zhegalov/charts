import * as am5 from "@amcharts/amcharts5"

export default class ChartTheme extends am5.Theme {
  setupDefaultRules() {
    this.rule("Label").setAll({
      fill: am5.color(0x000000),
      fontSize: "0.8em",
    })
    this.rule("ColorSet").setAll({
      colors: [
        am5.color("#3c428e"),
        am5.color("#7669ff"),
        am5.color("#fcac39"),
      ],
    })

    // Add shadow to all tooltips...
    this.rule("PointedRectangle", ["tooltip", "background"]).setAll({
      shadowColor: am5.color(0x222427),
      shadowBlur: 16,
      shadowOpacity: 0.2,
      shadowOffsetX: 8,
      shadowOffsetY: 8,
    })
  }
}

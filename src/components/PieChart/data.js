const dataSet = [
  {
    title: "Акции",
    value: 54,
    color: "#3c428e",
    children: [
      {
        title: "Акции развивающихся рынков",
        value: 25.43,
      },
      {
        title: "Акции развитых рынков",
        value: 28.57,
      },
    ],
  },
  {
    title: "Облигации",
    value: 26,
    color: "#7669ff",
    children: [
      {
        title: "Облигации развивающихся рынков",
        value: 11.6,
      },
      {
        title: "Высокодохожные облигации развитых рынков",
        value: 7.2,
      },
      {
        title: "Облигации инвестиционного уровня",
        value: 7.2,
      },
    ],
  },
  {
    title: "Альт. инвестиции",
    value: 20,
    color: "#fcac39",
    children: [
      {
        title: "Акции хедж-фондов",
        value: 10,
      },
      {
        title: "Что-то еще",
        value: 6,
      },
      {
        title: "И вот еще",
        value: 4,
      },
    ],
  },
]

export default dataSet
